<?php

namespace uuf6429\BitbucketReporter\Commands;

class Help extends AbstractCommand
{
    private $cmdInfos = [
        'help' => [
            'desc' => 'Displays helpful information about this application or for a command',
            'usage' => 'help [<command>]',
        ]
    ];

    public function run($self, $args)
    {
        $version = '1.0.0'; // TODO change as applicable
        $command = isset($args[0]) ? $args[0] : '';

        switch (true) {
            case $command === '':
                $this->console->writeln("Bitbucket Reporter version $version");
                $this->console->writeln();
                $this->console->writeln('Usage:');
                $this->console->writeln("  $self <command> [<arguments>]");
                $this->console->writeln();
                $this->console->writeln('Available commands:');
                $this->console->writeln('  help                 Shows this help screen');
                $this->console->writeln('  help <command>       Displays help for the specified command');
                $this->console->writeln('  prepare              Prepare bitbucket for a new set of reports (run ');
                $this->console->writeln('                       this at the beginning of your pipeline script)');
                $this->console->writeln('  submit               Generate reports from tests results and submit them ');
                $this->console->writeln('                       to bitbucket (run this in after_script)');
                break;

            case isset($this->cmdInfos[$command]):
                $this->console->writeln('Description:');
                $this->console->writeln('  ' . $this->cmdInfos[$command]['desc']);
                $this->console->writeln();
                $this->console->writeln('Usage:');
                $this->console->writeln("  $self {$this->cmdInfos[$command]['usage']}");
                break;

            default:
                $this->console->writeln("The specified command ($args[0]) does not exist.");
                break;
        }
    }
}
