<?php

namespace uuf6429\BitbucketReporter\Commands;

use uuf6429\BitbucketReporter\BitbucketApiClient;
use uuf6429\BitbucketReporter\TestResultsParser;

class Submit extends AbstractCommand
{
    public function run($self, $args)
    {
        static $defectTypeToAnnotationTypeMap = [
            'error' => BitbucketApiClient::ANNOTATION_BUG,
            'failure' => BitbucketApiClient::ANNOTATION_BUG,
            'risky' => BitbucketApiClient::ANNOTATION_CODE_SMELL,
            'skipped' => BitbucketApiClient::ANNOTATION_CODE_SMELL,
        ];
        static $defectTypeToResultTypeMap = [
            'error' => BitbucketApiClient::RESULT_FAILED,
            'failure' => BitbucketApiClient::RESULT_FAILED,
            'risky' => BitbucketApiClient::RESULT_IGNORED,
            'skipped' => BitbucketApiClient::RESULT_SKIPPED,
        ];
        static $defectTypeDefaultTextMap = [
            'risky' => 'This test was marked as risky',
            'skipped' => 'This test has been skipped',
        ];

        $metrics = [];
        $getMetric = static function ($name) use (&$metrics) {
            return isset($metrics[$name]) ? $metrics[$name] : null;
        };
        $getMetricPercent = static function ($name, $ofName) use (&$getMetric) {
            if ((int)($total = $getMetric($ofName)) === 0) {
                return 'n/a';
            }

            return floor($getMetric($name) / $total * 100);
        };
        $createCoverageDataEntry = function ($name) use (&$getMetricPercent) {
            return $this->client->createReportDataEntry(
                'Covered ' . ucfirst($name),
                BitbucketApiClient::VALUE_PERCENTAGE,
                $getMetricPercent("covered$name", $name)
            );
        };
        $getNumberDataEntry = function ($name, $label, $multiplier = 1) use (&$getMetric) {
            return $this->client->createReportDataEntry(
                $label ?: ucwords($name),
                BitbucketApiClient::VALUE_NUMBER,
                $getMetric($name) * $multiplier
            );
        };

        $resultsParser = new TestResultsParser($this->config, $this->console);
        $resultsParser->parse();

        // generate and send test result report
        $metrics = $resultsParser->getTestMetrics();
        $failureReason = $resultsParser->getTestFailureReason();
        $this->client->upsertReport("{$this->config->getClientAppId()}-tests", $this->client->createReportPayload(
            'PHPUnit Tests',
            'PHPUnit Test Results' . ($failureReason ? " - $failureReason" : ''),
            BitbucketApiClient::REPORT_TEST,
            $failureReason ? BitbucketApiClient::RESULT_FAILED : BitbucketApiClient::RESULT_PASSED,
            $metrics ? [
                $getNumberDataEntry('tests', 'Tests Run'),
                $getNumberDataEntry('assertions', 'Assertions '),
                $this->client->createReportDataEntry(
                    'Time Taken',
                    BitbucketApiClient::VALUE_DURATION,
                    $getMetric('time') * 1000
                ),
                $getNumberDataEntry('failures', 'Failures'),
                $getNumberDataEntry('errors', 'Errors'),
                $getNumberDataEntry('warnings', 'Warnings'),
                $getNumberDataEntry('skipped', 'Skipped'),
            ] : []
        ));
        $defectId = 0;
        $this->client->createAnnotations(
            "{$this->config->getClientAppId()}-tests",
            array_map(
                function ($defect) use (
                    &$defectId,
                    $defectTypeToAnnotationTypeMap,
                    $defectTypeToResultTypeMap,
                    $defectTypeDefaultTextMap
                ) {
                    list('file' => $file, 'line' => $line, 'text' => $text, 'type' => $type) = $defect;
                    $defectId++;
                    return $this->client->createReportAnnotationEntry(
                        "{$this->config->getClientAppId()}-test-$defectId",
                        null,
                        $defectTypeToAnnotationTypeMap[$type],
                        $text ?: $defectTypeDefaultTextMap[$type],
                        $defectTypeToResultTypeMap[$type],
                        null,
                        $file,
                        $line
                    );
                },
                $resultsParser->getTestDefects()
            )
        );

        // generate and send coverage result report
        if ($this->config->isCoverageReportingEnabled()) {
            $metrics = $resultsParser->getCoverageMetrics();
            $failureReason = $resultsParser->getCoverageFailureReason();
            $this->client->upsertReport("{$this->config->getClientAppId()}-cover", $this->client->createReportPayload(
                'PHPUnit Coverage',
                'PHPUnit Coverage Results' . ($failureReason ? " - $failureReason" : ''),
                BitbucketApiClient::REPORT_COVERAGE,
                $failureReason ? BitbucketApiClient::RESULT_FAILED : BitbucketApiClient::RESULT_PASSED,
                $metrics ? [
                    $getNumberDataEntry('file', 'Files'),
                    $getNumberDataEntry('classes', 'Classes'),
                    $getNumberDataEntry('loc', 'Lines Of Code '),
                    $this->client->createReportDataEntry(
                        'Total Coverage',
                        BitbucketApiClient::VALUE_PERCENTAGE,
                        $getMetric('tpc')
                    ),
                    $createCoverageDataEntry('methods'),
                    $createCoverageDataEntry('conditions'),
                    $createCoverageDataEntry('statements'),
                    $createCoverageDataEntry('elements'),
                ] : []
            ));
            $uncoveredLineId = 0;
            $this->client->createAnnotations(
                "{$this->config->getClientAppId()}-cover",
                array_map(
                    function ($uncoveredLine) use (&$uncoveredLineId) {
                        list($line, $file) = array_map('strrev', explode(':', strrev($uncoveredLine), 2));
                        $uncoveredLineId++;
                        return $this->client->createReportAnnotationEntry(
                            "{$this->config->getClientAppId()}-cover-$uncoveredLineId",
                            null,
                            BitbucketApiClient::ANNOTATION_CODE_SMELL,
                            'Not covered',
                            BitbucketApiClient::RESULT_FAILED,
                            null,
                            $file,
                            $line
                        );
                    },
                    array_slice($resultsParser->getUncoveredLines(), 0, 100) // TODO we can add up to 1000, in batches
                )
            );
        }
    }
}
