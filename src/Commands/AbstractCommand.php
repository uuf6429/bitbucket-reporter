<?php

namespace uuf6429\BitbucketReporter\Commands;

use uuf6429\BitbucketReporter\BitbucketApiClient;
use uuf6429\BitbucketReporter\Config;
use uuf6429\BitbucketReporter\Console;

abstract class AbstractCommand
{
    /**
     * @var Console
     */
    protected $console;

    /**
     * @var BitbucketApiClient
     */
    protected $client;

    /**
     * @var Config
     */
    protected $config;

    public function __construct(Console $console, BitbucketApiClient $client, Config $config)
    {
        $this->console = $console;
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @return static
     */
    public static function create(Console $console, BitbucketApiClient $client, Config $config)
    {
        return new static($console, $client, $config);
    }

    /**
     * @param string $self
     * @param array<string, string> $args
     */
    abstract public function run($self, $args);
}
