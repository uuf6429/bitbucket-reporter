<?php

namespace uuf6429\BitbucketReporter\Commands;

use uuf6429\BitbucketReporter\BitbucketApiClient;

class Prepare extends AbstractCommand
{
    public function run($self, $args)
    {
        // clear existing reports
        $this->client->deleteReport("{$this->config->getClientAppId()}-tests");
        $this->client->deleteReport("{$this->config->getClientAppId()}-cover");

        // send pending tests report
        $this->client->upsertReport(
            "{$this->config->getClientAppId()}-tests",
            $this->client->createReportPayload(
                'PHPUnit Tests',
                'PHPUnit Test Results (pending)',
                BitbucketApiClient::REPORT_TEST,
                BitbucketApiClient::RESULT_PENDING,
                []
            )
        );

        // send pending coverage report
        if ($this->config->isCoverageReportingEnabled()) {
            $this->client->upsertReport(
                "{$this->config->getClientAppId()}-cover",
                $this->client->createReportPayload(
                    'PHPUnit Coverage',
                    'PHPUnit Coverage Results (pending)',
                    BitbucketApiClient::REPORT_COVERAGE,
                    BitbucketApiClient::RESULT_PENDING,
                    []
                )
            );
        }
    }
}
