<?php

namespace uuf6429\BitbucketReporter;

use ErrorException;

class Application
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Console
     */
    private $console;

    /**
     * @var BitbucketApiClient
     */
    private $client;

    public function __construct()
    {
        $this->config = new Config();
        $this->console = new Console();
        $this->client = new BitbucketApiClient($this->config, $this->console);

        set_error_handler(function ($errno, $errstr, $errfile = 'unknown', $errline = 0) {
            $this->console->warn(new ErrorException($errstr, $errno, $errno, $errfile, $errline));
        });

        set_exception_handler(function ($exception) {
            $this->console->fail($exception);
            $this->console->writeln();
            exit(1);
        });
    }

    /**
     * @return int
     */
    public function run()
    {
        list($self, $cmd, $args) = $this->console->parseCliArgs();

        switch ($cmd) {
            case 'help':
                Commands\Help::create($this->console, $this->client, $this->config)->run($self, $args);
                break;

            case 'prepare':
                Commands\Prepare::create($this->console, $this->client, $this->config)->run($self, $args);
                break;

            case 'submit':
                Commands\Submit::create($this->console, $this->client, $this->config)->run($self, $args);
                break;

            default:
                Commands\Help::create($this->console, $this->client, $this->config)->run($self, $args);
                $this->console->writeln();
                return 1;
        }

        $this->console->writeln();
        return 0;
    }
}
