<?php

namespace uuf6429\BitbucketReporter;

class Config
{
    /**
     * Token for using the Bitbucket API.
     * It is actually an "App Password" with a "Read Repositories" scope.
     * @see https://support.atlassian.com/bitbucket-cloud/docs/code-insights/#Authentication:~:text=For%20the%20Reports%2DAPI
     * @var null|string
     */
    private $apiAccessToken = null;

    /**
     * Proxy used to access the Bitbucket API.
     * When null and $accessToken is also null, it defaults to the Bitbucket auth proxy.
     * @see https://support.atlassian.com/bitbucket-cloud/docs/code-insights/#Authentication
     * @var null|string
     */
    private $apiAccessProxy = null;

    /**
     * @var float
     */
    private $minimumAllowedCoveragePercent = 80.0;

    /**
     * @var bool
     */
    private $coverageReporting = true;

    /**
     * @var null|string
     */
    private $repoFullName;

    /**
     * @var null|string
     */
    private $commitHash;

    /**
     * @var string[]
     */
    private $testResultSearchPaths = [
        './**/surefire-reports/**/*.xml',
        './**/failsafe-reports/**/*.xml',
        './**/test-results/**/*.xml',
        './**/test-reports/**/*.xml',
        './**/TestResults/**/*.xml',
    ];

    /**
     * @return null|string
     */
    public function getApiAccessToken()
    {
        return $this->apiAccessToken;
    }

    /**
     * @param null|string $accessToken
     */
    public function setApiAccessToken($accessToken)
    {
        $this->apiAccessToken = $accessToken;
    }

    /**
     * @return null|string
     */
    public function getApiAccessProxy()
    {
        if ($this->apiAccessToken === null && $this->apiAccessProxy === null) {
            // See: https://github.com/reviewdog/reviewdog/blob/02b71076ef380478602006378e4cbd27dafbcdc6/cienv/bitbucket_pipelines.go#L12
            $isInBitbucketPipe = getenv('BITBUCKET_PIPE_STORAGE_DIR') || getenv('BITBUCKET_PIPE_SHARED_STORAGE_DIR');
            return $isInBitbucketPipe ? 'http://host.docker.internal:29418' : 'http://localhost:29418';
        }

        return $this->apiAccessProxy;
    }

    /**
     * @return string
     */
    public function getClientAppName()
    {
        return 'Bitbucket Reporter';
    }

    /**
     * @return string
     */
    public function getClientAppId()
    {
        return 'bbr';
    }

    /**
     * @return float
     */
    public function getMinimumAllowedCoveragePercent()
    {
        return $this->minimumAllowedCoveragePercent;
    }

    /**
     * @return bool
     */
    public function isCoverageReportingEnabled()
    {
        return $this->coverageReporting;
    }

    /**
     * @return string|null
     */
    public function getRepoFullName()
    {
        return $this->repoFullName === null ? getenv('BITBUCKET_REPO_FULL_NAME') : $this->repoFullName;
    }

    /**
     * @return string|null
     */
    public function getCommitHash()
    {
        return $this->commitHash === null ? getenv('BITBUCKET_COMMIT') : $this->commitHash;
    }

    /**
     * @return string[]
     */
    public function getTestResultSearchPaths()
    {
        return $this->testResultSearchPaths;
    }
}
