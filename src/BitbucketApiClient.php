<?php

namespace uuf6429\BitbucketReporter;

use JetBrains\PhpStorm\ExpectedValues;

class BitbucketApiClient
{
    const REPORT_SECURITY = 'SECURITY';
    const REPORT_COVERAGE = 'COVERAGE';
    const REPORT_TEST = 'TEST';
    const REPORT_BUG = 'BUG';

    const RESULT_NONE = null;
    const RESULT_PASSED = 'PASSED';
    const RESULT_FAILED = 'FAILED';
    const RESULT_PENDING = 'PENDING';
    const RESULT_IGNORED = 'IGNORED';
    const RESULT_SKIPPED = 'SKIPPED';

    const ANNOTATION_VULNERABILITY = 'VULNERABILITY';
    const ANNOTATION_CODE_SMELL = 'CODE_SMELL';
    const ANNOTATION_BUG = 'BUG';

    const SEVERITY_NONE = null;
    const SEVERITY_HIGH = 'HIGH';
    const SEVERITY_MEDIUM = 'MEDIUM';
    const SEVERITY_LOW = 'LOW';
    const SEVERITY_CRITICAL = 'CRITICAL';

    const VALUE_NONE = null;
    const VALUE_BOOLEAN = 'BOOLEAN';
    const VALUE_DATE = 'DATE';
    const VALUE_DURATION = 'DURATION';
    const VALUE_LINK = 'LINK';
    const VALUE_NUMBER = 'NUMBER';
    const VALUE_PERCENTAGE = 'PERCENTAGE';
    const VALUE_TEXT = 'TEXT';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Console
     */
    private $console;

    /**
     * @see https://community.atlassian.com/t5/Bitbucket-questions/Why-can-t-I-post-a-custom-code-insight/qaq-p/1412912#M55938
     * @var string
     */
    private $baseUrl = 'http://api.bitbucket.org/2.0';

    public function __construct(Config $config, Console $console)
    {
        $this->config = $config;
        $this->console = $console;
    }

    /**
     * @param string $title
     * @param string $details
     * @param string $reportType
     * @param string $resultType
     * @param array $reportDataEntries
     * @return array
     */
    public function createReportPayload(
        $title,
        $details,
        #[ExpectedValues([self::REPORT_SECURITY, self::REPORT_COVERAGE, self::REPORT_TEST, self::REPORT_BUG])]
        $reportType,
        #[ExpectedValues([self::RESULT_PASSED, self::RESULT_FAILED, self::RESULT_PENDING])]
        $resultType,
        $reportDataEntries
    )
    {
        return [
            'title' => $title,
            'details' => $details,
            'report_type' => $reportType,
            'reporter' => $this->config->getClientAppName(),
            'result' => $resultType,
            'remote-link-enabled' => true,
            'data' => $reportDataEntries,
        ];
    }

    /**
     * @param string $title
     * @param string $type
     * @param mixed $value
     * @return array
     */
    public function createReportDataEntry(
        $title,
        #[ExpectedValues([self::VALUE_NONE, self::VALUE_BOOLEAN, self::VALUE_DATE, self::VALUE_DURATION, self::VALUE_LINK, self::VALUE_NUMBER, self::VALUE_PERCENTAGE, self::VALUE_TEXT])]
        $type = null,
        $value = null
    )
    {
        if (is_string($value) && in_array($type, [self::VALUE_NUMBER, self::VALUE_PERCENTAGE], true)) {
            $type = self::VALUE_TEXT;
        }

        return array_merge(['title' => $title], $type !== null ? ['type' => $type, 'value' => $value] : []);
    }

    /**
     * @param string $annotationId
     * @param string|null $title
     * @param string $annotationType
     * @param string $summary
     * @param string|null $resultType
     * @param string|null $severityType
     * @param string|null $path
     * @param int|null $line
     * @return array
     */
    public function createReportAnnotationEntry(
        $annotationId,
        $title,
        #[ExpectedValues([self::ANNOTATION_VULNERABILITY, self::ANNOTATION_CODE_SMELL, self::ANNOTATION_BUG])]
        $annotationType,
        $summary,
        #[ExpectedValues([self::RESULT_NONE, self::RESULT_PASSED, self::RESULT_FAILED, self::RESULT_IGNORED, self::RESULT_SKIPPED])]
        $resultType,
        #[ExpectedValues([self::SEVERITY_NONE, self::SEVERITY_HIGH, self::SEVERITY_MEDIUM, self::SEVERITY_LOW, self::SEVERITY_CRITICAL])]
        $severityType,
        $path,
        $line
    )
    {
        $result = ['external_id' => $annotationId, 'annotation_type' => $annotationType, 'summary' => $summary];
        if ($title !== null) {
            $result['title'] = $title;
        }
        if ((string)$resultType !== '') {
            $result['result'] = $resultType;
        }
        if ((string)$severityType !== '') {
            $result['severity'] = $severityType;
        }
        if ((string)$path !== '') {
            $result['path'] = $path;
        }
        if ((string)$path !== '' && (string)$line !== '') {
            $result['line'] = $line;
        }

        return $result;
    }

    /**
     * @param string $reportId
     */
    public function deleteReport($reportId)
    {
        $url = sprintf(
            '%s/repositories/%s/commit/%s/reports/%s',
            $this->baseUrl,
            $this->config->getRepoFullName(),
            $this->config->getCommitHash(),
            $reportId
        );
        $this->request('DELETE', $url, null);
    }

    /**
     * @param string $reportId
     * @param array $report
     */
    public function upsertReport($reportId, array $report)
    {
        $url = sprintf(
            '%s/repositories/%s/commit/%s/reports/%s',
            $this->baseUrl,
            $this->config->getRepoFullName(),
            $this->config->getCommitHash(),
            $reportId
        );
        $this->request('PUT', $url, $report);
    }

    /**
     * @param string $reportId
     * @param array $annotations
     */
    public function createAnnotations($reportId, array $annotations)
    {
        if (!count($annotations)) {
            $this->console->info("No annotations for report $reportId have been specified");
            return;
        }

        $url = sprintf(
            '%s/repositories/%s/commit/%s/reports/%s/annotations',
            $this->baseUrl,
            $this->config->getRepoFullName(),
            $this->config->getCommitHash(),
            $reportId
        );
        $this->request('POST', $url, array_reverse($annotations));
    }

    /**
     * @param string $method
     * @param string $url
     * @param mixed $payloadData
     */
    private function request($method, $url, $payloadData)
    {
        $ch = curl_init($url);
        try {
            $options = [
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADERFUNCTION => static function ($curl, $header) use (&$headers) {
                    $headers .= $header;
                    return strlen($header);
                },
            ];
            $payload = '<no payload>';
            if ($payloadData) {
                $options[CURLOPT_POSTFIELDS] = ($payload = json_encode($payloadData));
                $options[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($payload);
                $payload = json_encode($payloadData, JSON_PRETTY_PRINT);
            }
            if ($accessToken = $this->config->getApiAccessToken()) {
                $options[CURLOPT_HTTPHEADER][] = "Authorization: Bearer $accessToken";
            }
            if ($accessProxy = $this->config->getApiAccessProxy()) {
                $options[CURLOPT_PROXY] = $accessProxy;
            }

            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);

            $this->console->info("$method $url\n$payload\n$headers$result");
        } finally {
            curl_close($ch);
        }
    }
}
