<?php

namespace uuf6429\BitbucketReporter;

use JetBrains\PhpStorm\ArrayShape;

class Console
{
    /**
     * @param string $message
     */
    public function write($message)
    {
        file_put_contents('php://stdout', $message);
    }

    /**
     * @param string $message
     */
    public function writeln($message = '')
    {
        $this->write($message . PHP_EOL);
    }

    public function log($level, $message)
    {
        $prefix = sprintf('%s [%s] ', date('c'), $level);
        $indent = str_pad('', strlen($prefix));
        $this->writeln($prefix . str_replace("\n", "\n$indent", $message));
    }

    public function info($message)
    {
        $this->log('info', $message);
    }

    public function warn($message)
    {
        $this->log('warn', $message);
    }

    public function fail($message)
    {
        $this->log('fail', $message);
    }

    #[ArrayShape([0 => 'string', 1 => 'string', 2 => 'array'])]
    public function parseCliArgs()
    {
        $argv = isset($_SERVER['argv']) ? $_SERVER['argv'] : [];
        $self = basename(array_shift($argv));
        $comm = array_shift($argv);
        $args = [];
        $lastArg = null;
        foreach ($argv as $arg) {
            $split = explode('--', $arg, 2);
            if ($split[0] === '' && isset($split[1])) {
                $args[$split[1]] = true;
                $lastArg = $split[1];
            } elseif ($lastArg !== null) {
                $args[$lastArg] = $arg;
                $lastArg = null;
            } else {
                $args[0] = $arg;
            }
        }

        return [$self, $comm, $args];
    }
}
