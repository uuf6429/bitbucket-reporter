<?php

namespace uuf6429\BitbucketReporter;

use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionMethod;

class TestResultsParser
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Console
     */
    private $console;

    private $hasParsedTestMetrics = false;

    private $parsedTestMetrics = [
        'tests' => 0,
        'assertions' => 0,
        'errors' => 0,
        'warnings' => 0,
        'failures' => 0,
        'skipped' => 0,
        'time' => 0.0,
    ];

    /**
     * @var array{file: string, line: int, text: string, type: string}[]
     */
    private $parsedTestDefects = [];

    private $hasParsedCoverageMetrics = false;

    private $parsedCoverageMetrics = [
        'files' => 0,
        'loc' => 0, // lines of code
        'ncloc' => 0, // non-commented loc
        'tpc' => 0.0, // total percentage of coverage
        'classes' => 0,
        'methods' => 0,
        'coveredmethods' => 0,
        'conditionals' => 0,
        'coveredconditionals' => 0,
        'statements' => 0,
        'coveredstatements' => 0,
        'elements' => 0,
        'coveredelements' => 0,
    ];

    /**
     * @var array<string, bool>
     */
    private $parsedCoverage = [];

    public function __construct(Config $config, Console $console)
    {
        $this->config = $config;
        $this->console = $console;
    }

    public function parse()
    {
        foreach ($this->config->getTestResultSearchPaths() as $path) {
            $this->console->info("Searching in `$path`...");
            foreach ($this->globstar($path) as $file) {
                $this->console->info("Found: $file");
                $doc = new DOMDocument();
                $doc->load($file);
                $xpath = new DOMXPath($doc);
                switch (true) {
                    //TODO find a better way to identify the files (e.g. by contents)
                    case basename($file) === 'junit.xml':
                        $this->parseJUnitFile($xpath);
                        break;

                    //TODO find a better way to identify the files (e.g. by contents)
                    case basename($file) === 'clover.xml':
                        $this->parseCloverFile($xpath);
                        break;

                    default:
                        $this->console->warn("A test file could not be recognized and has bee skipped: $file");
                }
            }
        }
    }

    /**
     * @return null|array
     */
    #[ArrayShape(['tests' => 'int', 'assertions' => 'int', 'errors' => 'int', 'warnings' => 'int', 'failures' => 'int', 'skipped' => 'int', 'time' => 'int'])]
    public function getTestMetrics()
    {
        return $this->hasParsedTestMetrics ? $this->parsedTestMetrics : null;
    }

    /**
     * @return null|string
     */
    public function getTestFailureReason()
    {
        switch (true) {
            case !$this->hasParsedTestMetrics:
                return 'No test result file(s) found';

            case $this->parsedTestMetrics['errors'] || $this->parsedTestMetrics['failures']:
                return 'Some tests failed or errored';

            default:
                return null;
        }
    }

    /**
     * @return array{file: string, line: int, text: string}[]
     */
    public function getTestDefects()
    {
        return $this->parsedTestDefects;
    }

    /**
     * @return null|array
     */
    #[ArrayShape(['files' => 'int', 'loc' => 'int', 'ncloc' => 'int', 'tpc' => 'float', 'classes' => 'int', 'methods' => 'int', 'coveredmethods' => 'int', 'conditionals' => 'int', 'coveredconditionals' => 'int', 'statements' => 'int', 'coveredstatements' => 'int', 'elements' => 'int', 'coveredelements' => 'int'])]
    public function getCoverageMetrics()
    {
        // See: https://confluence.atlassian.com/clover/about-clover-code-metrics-306350640.html (couldn't find a more reliable source)
        $tpc = ($this->parsedCoverageMetrics['coveredmethods']
                + $this->parsedCoverageMetrics['coveredconditionals']
                + $this->parsedCoverageMetrics['coveredstatements']
                + $this->parsedCoverageMetrics['coveredelements']
            ) / (
                $this->parsedCoverageMetrics['methods']
                + $this->parsedCoverageMetrics['conditionals']
                + $this->parsedCoverageMetrics['statements']
                + $this->parsedCoverageMetrics['elements']
            ) * 100;

        return $this->hasParsedCoverageMetrics ? $this->parsedCoverageMetrics + ['tpc' => $tpc] : null;
    }

    /**
     * @return null|string
     */
    public function getCoverageFailureReason()
    {
        $minAllowedCoverage = $this->config->getMinimumAllowedCoveragePercent();
        if ($this->getCoverageMetrics()['tpc'] < $this->config->getMinimumAllowedCoveragePercent()) {
            return "Covered less than the allowed minimum of $minAllowedCoverage%";
        }

        return null;
    }

    /**
     * @return string[]
     */
    public function getUncoveredLines()
    {
        return array_keys(array_filter(
            $this->parsedCoverage,
            static function ($covered) {
                return !$covered;
            }
        ));
    }

    private function parseJUnitFile(DOMXPath $doc)
    {
        $this->hasParsedTestMetrics = true;

        /** @var DOMNodeList|DOMElement[] $nodes */
        $nodes = $doc->evaluate('/testsuites/testsuite');
        foreach ($nodes as $node) {
            foreach (array_keys($this->parsedTestMetrics) as $key) {
                $value = $node->getAttribute($key) ?: '0';
                settype($value, gettype($this->parsedTestMetrics[$key]));
                $this->parsedTestMetrics[$key] += $value;
            }
        }

        /** @var DOMNodeList|DOMElement[] $nodes */
        $nodes = $doc->evaluate('//error | //failure | //skipped');
        $rootPathLength = strlen(getcwd()) + 1;
        foreach ($nodes as $node) {
            $parent = $node->parentNode;
            if (!$parent instanceof DOMElement) {
                continue;
            }

            $text = trim($node->nodeValue);

            if (preg_match(
                '/^(?<title>[^\n]+)\n(?<message>.+)\n(?<stack>(?<root>\n[^\n]+:\d+)(?:\n[^\n]+:\d+)*)$/s',
                $text,
                $match
            )) {
                $rootCause = array_reverse(array_map('strrev', explode(':', strrev($match['root']), 2)));
                $defect = [
                    'file' => ltrim(substr($rootCause[0], $rootPathLength), '/'),
                    'line' => isset($rootCause[1]) ? (int)$rootCause[1] : 0,
                    'text' => trim($match['message']),
                ];
            } else {
                $defect = [
                    'file' => ltrim(substr($parent->getAttribute('file'), $rootPathLength), '/'),
                    'line' => (int)$parent->getAttribute('line'),
                    'text' => $text,
                ];
            }

            if ($defect['file'] === '' && $defect['line'] === 0) {
                try {
                    /** @var DOMElement $grandparent */
                    $grandparent = $parent->parentNode;
                    $reflector = new ReflectionMethod($grandparent->getAttribute('name'));
                    $defect['file'] = ltrim(substr($reflector->getFileName(), $rootPathLength), '/');
                    $defect['line'] = (int)$reflector->getStartLine();
                } catch (Exception $ex) {
                    $this->console->warn("Could not detect test defect location: $ex");
                }
            }

            $defect['type'] = strtolower($node->nodeName);

            $this->parsedTestDefects[] = $defect;
        }
    }

    private function parseCloverFile(DOMXPath $doc)
    {
        $this->hasParsedCoverageMetrics = true;

        /** @var DOMNodeList|DOMElement[] $nodes */
        $nodes = $doc->evaluate('/coverage/project/metrics');
        foreach ($nodes as $node) {
            foreach (array_keys($this->parsedCoverageMetrics) as $key) {
                $value = $node->getAttribute($key) ?: '0';
                settype($value, gettype($this->parsedCoverageMetrics[$key]));
                $this->parsedCoverageMetrics[$key] += $value;
            }
        }

        $rootPathLength = strlen(getcwd()) + 1;
        /** @var DOMNodeList|DOMElement[] $nodes */
        $nodes = $doc->evaluate('//line[@type="stmt"]');
        foreach ($nodes as $node) {
            $parent = $node->parentNode;
            if (!$parent instanceof DOMElement) {
                continue;
            }

            $file = ltrim(substr($parent->getAttribute('name'), $rootPathLength), '/');
            $location = "$file:{$node->getAttribute('num')}";
            $covered = $node->getAttribute('count') !== '0';

            if (!isset($this->parsedCoverage[$location])) {
                $this->parsedCoverage[$location] = false;
            }
            $this->parsedCoverage[$location] = $this->parsedCoverage[$location] || $covered;
        }
    }

    private function globstar($pattern, $flags = 0)
    {
        $files = [];
        $this->globstarBuffered($pattern, $flags, $files);
        $files = array_unique(array_merge(...$files));
        sort($files);
        return $files;
    }

    /**
     * This function is quite heavy (depending on the patterns passed)...maybe it could be further optimised?
     * @internal Used by {@see self::globstar()]
     */
    private function globstarBuffered($pattern, $flags, &$buffer)
    {
        if (substr($pattern, 0, 2) === '**') {
            $pattern = './' . $pattern;
        }

        if (strpos($pattern, '**') === false) {
            $buffer[] = glob($pattern, $flags);
            return;
        }

        list($rootPattern, $restPattern) = explode('**', $pattern, 2);
        $rootPattern = substr($rootPattern, 0, -1);

        $this->globstarBuffered($rootPattern . $restPattern, $flags, $buffer);

        $rootPattern .= '/*';
        while ($dirs = glob($rootPattern, GLOB_ONLYDIR)) {
            $rootPattern .= '/*';
            foreach ($dirs as $dir) {
                $this->globstarBuffered($dir . $restPattern, $flags, $buffer);
            }
        }
    }
}
