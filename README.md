# Bitbucket Reporter

A tool that creates and submits Bitbucket Code Insights reports from (PHPUnit) JUnit/Clover test results.

## Installation

The easiest way to install this is using [Composer](https://getcomposer.org):

```shell
composer require uuf6429/bitbucket-reporter
```

_Note: typically this application is only needed during development/testing, so it is recommended that you run the
previous composer command with `--dev`._

**Alternatively**, you can also
just [download `bitbucket-reporter.phar` from Bitbucket](https://bitbucket.org/uuf6429/bitbucket-reporter/downloads/). To make
it convenient, you can remove the '.phar' extension, move the file to a `$PATH` bin directory (e.g. `/usr/local/bin`) nd
made executable (`chmod +x`).

## Usage

This tool is run from the command line. By default, no configuration is necessary, but this assumes a specific setup
(coverage is enabled and JUnit/Clover test result files are in
a [path as recognised by Bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines/#Configuring-test-reporting))
.
How the tool is run from the command line depends on how it has been installed:

1. Composer: `composer bitbucket-reporter`
2. PHAR in Project: `php bitbucket-reporter.phar`
3. PHAR in Bin/$PATH: `bitbucket-reporter`

_In the examples below, we assume the 2nd option was chosen._

The tool provides two commands with some optional options/arguments:

- `prepare`: deletes existing reports and adds new ones in pending state
    - `--no-coverage`: avoids creation of a coverage report in pending state
- `submit`: generates and submit new reports based on JUnit and Clover files
    - `--no-coverage`: missing coverage file will not result in a failure and also avoids creation of a coverage report

## FAQ

### Where do I set up the repository name, auth token etc.?

This tool uses environment variables defined by Bitbucket (and the Bitbucket auth proxy), but you can overwrite these
from the command line.

### Why does the test results have to be in a path known to Bitbucket?

Doing so enable Bitbucket to display test results in its own interface - and is thus the default desirable behaviour.
You can always override the paths from the command line though.

### Does this work on Bitbucket Server / Enterprise / ..?

No idea. This was designed to work on Bitbucket Cloud. If you know how it can be done for those, please do get in touch.

### Why is all the code custom (e.g. console, logging...)?

The purpose of this tool is and will remain very simple and straightforward, therefore using feature-complete 3rd-party
libraries would only add to the bloat and risk dependency conflicts.
