<?php

namespace uuf6429\BitbucketReporter;

use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    public function testThatSettingAnAuthTokenDisablesAuthProxy()
    {
        $config = new Config();

        $config->setApiAccessToken('test');

        $this->assertNull($config->getApiAccessProxy());
    }

    public function testSlowTestIsReport()
    {
        usleep(1.5e+6);
        $this->assertTrue(true);
    }

    public function testEmptyTest()
    {
    }

    public function testOutputTest()
    {
        $this->assertTrue(true);
        echo 'something';
    }

    public function testSkippedTest()
    {
        $this->markTestSkipped('Skipped a test');
    }

    public function testIncompleteTest()
    {
        $this->markTestIncomplete('Todo');
    }

    public function testRiskyTest()
    {
        $this->assertTrue(true);
        $this->markAsRisky();
    }
}
